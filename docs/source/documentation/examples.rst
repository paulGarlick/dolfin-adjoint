.. _dolfin-adjoint-examples:

********************************
Examples of using dolfin-adjoint
********************************

Sensitivity analysis
--------------------

.. toctree::

   klein/klein

Optimization examples
---------------------

.. toctree::

   poisson-mother/poisson-mother
   stokes-bc-control/stokes-bc-control
   poisson-topology/poisson-topology
   stokes-topology/stokes-topology
   time-dependent-wave/time-dependent-wave
   time-distributed-control/time-distributed-control
   mpec/mpec

Generalized stability examples
------------------------------

.. toctree::

   salt-fingering/salt-fingering
